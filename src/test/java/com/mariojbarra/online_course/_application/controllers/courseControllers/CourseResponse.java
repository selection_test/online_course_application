package com.mariojbarra.online_course._application.controllers.courseControllers;

import com.mariojbarra.online_course._application.core.Level;
import com.mariojbarra.online_course._application.core.Status;

import java.util.Date;

public class CourseResponse {
    private String courseName;
    private Level courseLevel;
    private int courseDuration;
    private Status status;

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public Level getCourseLevel() {
        return courseLevel;
    }

    public void setCourseLevel(Level courseLevel) {
        this.courseLevel = courseLevel;
    }

    public int getCourseDuration() {
        return courseDuration;
    }

    public void setCourseDuration(int courseDuration) {
        this.courseDuration = courseDuration;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date startDate;

    @Override
    public String toString() {
        return "CourseResponse{" +
                "courseName='" + courseName + '\'' +
                ", courseLevel=" + courseLevel +
                ", courseDuration=" + courseDuration +
                ", status=" + status +
                ", startDate=" + startDate +
                '}';
    }
}
