package com.mariojbarra.online_course._application.controllers.courseControllers;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.mariojbarra.online_course._application.controllers.userControllers.UserRequestBody;
import com.mariojbarra.online_course._application.controllers.userControllers.UserResponse;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

class CreateCourseEndpointTest {
    ObjectMapper mapper = new ObjectMapper().registerModule(new JavaTimeModule());

    @Test
    void should_create_user() throws IOException {

        List<UserResponse> courses = getUserFromServer();

        UserRequestBody newUser = new UserRequestBody();
        newUser.setUserName("Test2-" + UUID.randomUUID());


        URL creqtionUrl = new URL("http://localhost:8080/users");

        HttpURLConnection creationConnection = (HttpURLConnection) creqtionUrl.openConnection();
        creationConnection.setRequestProperty("Content-Type", "application/json");
        creationConnection.setRequestMethod("POST");
        creationConnection.setDoOutput(true);
        String requestBody = mapper.writeValueAsString(newUser);
        System.out.println(requestBody);
        creationConnection.getOutputStream().write(requestBody.getBytes("UTF-8"));

        InputStream createResponseStream = creationConnection.getInputStream();


        List<UserResponse> usersAfterCreate = getUserFromServer();

        assertThat(usersAfterCreate).hasSize(courses.size() + 1);

        System.out.println(usersAfterCreate);
        assertThat(usersAfterCreate).last().satisfies(last -> assertThat(last.getUserName()).isEqualTo(newUser.getUserName()));
    }

    private List<UserResponse> getUserFromServer() throws IOException {

        URL url = new URL("http://localhost:8080/users");

        HttpURLConnection connection = (HttpURLConnection) url.openConnection();

        connection.setRequestProperty("accept", "application/json");

        InputStream responseStream = connection.getInputStream();
        String text = new String(responseStream.readAllBytes(), StandardCharsets.UTF_8);


        System.out.println(text);
        List<UserResponse> users = mapper.readValue(responseStream, new TypeReference<List<UserResponse>>() {
        });
        return users;
    }
}

