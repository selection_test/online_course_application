package com.mariojbarra.online_course._application.core;

public enum Level {
    BASIC, MEDIUM, ADVANCED
}
