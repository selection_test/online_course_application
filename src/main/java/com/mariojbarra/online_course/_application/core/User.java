package com.mariojbarra.online_course._application.core;

import jakarta.persistence.*;

import java.util.List;

@Entity
@Table(name = "courseUser")
public class User {

    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "userName")
    private String userName;

    @ManyToMany(mappedBy = "alumnos")
    private List<Course> userCourses;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public List<Course> getUserCourses() {
        return userCourses;
    }

    public void setUserCourses(List<Course> userCourses) {
        this.userCourses = userCourses;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", userName='" + userName + '\'' +
                ", userCourses=" + userCourses +
                '}';
    }
}
