package com.mariojbarra.online_course._application.core;

import jakarta.persistence.*;

import java.time.Duration;
import java.time.LocalDate;
import java.util.List;

@Entity
@Table(name = "courses")
public class Course {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String courseName;
    private Level courseLevel;
    private int courseDuration;
    private Status status;
    public LocalDate startDate;

    @ManyToMany
    @JoinTable(
            name = "course_users",
            joinColumns = {@JoinColumn(name = "course_id")},
            inverseJoinColumns = {@JoinColumn(name = "user_id")}
    )
    private List<User> alumnos;

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public Level getCourseLevel() {
        return courseLevel;
    }

    public void setCourseLevel(Level courseLevel) {
        this.courseLevel = courseLevel;
    }

    public int getCourseDuration() {
        return courseDuration;
    }

    public void setCourseDuration(int courseDuration) {
        this.courseDuration = courseDuration;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public LocalDate getStartDate() {
        return startDate;
    }
    public LocalDate getEndDate() {
        return startDate.plus(Duration.ofDays(courseDuration));
    }
    public String getCourseStatus() {

        LocalDate now = LocalDate.now();
        LocalDate endOfCourse = startDate.plus(Duration.ofDays(courseDuration));

        if (now.isAfter(startDate) && now.isBefore(endOfCourse)) {
            return "En curso";
        } else if (now.isBefore(startDate)) {
            return "Pendiente de inicio";
        } else {
            return "Terminado";
        }
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }
}
