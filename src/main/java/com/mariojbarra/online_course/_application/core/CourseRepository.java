package com.mariojbarra.online_course._application.core;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface CourseRepository extends JpaRepository<Course, Long> {

    Optional<Course>findByCourseLevel(Level targetLevel);


    Course findByCourseName(String currentName);
}
