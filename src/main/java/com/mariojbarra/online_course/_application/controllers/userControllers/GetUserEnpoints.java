package com.mariojbarra.online_course._application.controllers.userControllers;

import com.mariojbarra.online_course._application.core.User;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class GetUserEnpoints {
    private final UserService userService;

    public GetUserEnpoints(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/user")
    public List<User> allUsers() {

       return userService.getAll();

    }

    @GetMapping("/user/{id}")
    public ResponseEntity<Object> userById(@PathVariable("id") Long userId) {

        return userService.getById(userId)
                .map(user -> ResponseEntity.ok(user))
                .orElseGet(() -> ResponseEntity.notFound().build());

    }

    @PutMapping("/user/{id}")
    public void modifyUser(@PathVariable("id") Long id, @RequestBody User persona) {

        userService.changeUser(persona);

        //Todo implement method
    }

    //TODO endpoint to list user
}

