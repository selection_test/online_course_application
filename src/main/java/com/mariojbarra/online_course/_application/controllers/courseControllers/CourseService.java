package com.mariojbarra.online_course._application.controllers.courseControllers;

import com.mariojbarra.online_course._application.core.Course;
import com.mariojbarra.online_course._application.core.CourseRepository;
import com.mariojbarra.online_course._application.core.Level;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

@Service
public class CourseService {
    private final CourseRepository courseRepository;

    public CourseService(CourseRepository courseRepository) {
        this.courseRepository = courseRepository;
    }

    public Course createCourse(CourseRequestBody newCourse) {
        Course courseToCreate = new Course();
        courseToCreate.setCourseName(newCourse.getCourseName());
        courseToCreate.setCourseLevel(newCourse.getCourseLevel());
        courseToCreate.setStatus(newCourse.getStatus());
        courseToCreate.setCourseDuration(newCourse.getCourseDuration());
        courseToCreate.setStartDate(newCourse.getStartDate());

        return courseRepository.save(courseToCreate);
    }

    public List<Course> getAll() {

        List<Course> allcourses = courseRepository.findAll();

       Collections.sort(allcourses, Comparator.comparing(Course::getCourseName));

        return allcourses;
    }


        /*for (int i = 0; i < allcourses.size(); i++) {
            String nameCurrentCourse = allcourses.get(i).getCourseName();
            alfabeticCourses.add(nameCurrentCourse);
        }
        Collections.sort(alfabeticCourses);
        return alfabeticCourses;*/



    public Optional<Course> getForLevel(@PathVariable Level targetLevel) {

        return courseRepository.findByCourseLevel(targetLevel);
    }


    public Course getForName(@PathVariable String currentName) {
        return courseRepository.findByCourseName(currentName);
    }

    public void changeCourse(Course currentCourse) {
        Course courseToChange = courseRepository.findByCourseName(currentCourse.getCourseName());

        courseRepository.delete(courseToChange);
        Course modificatedCourse = new Course();
        courseRepository.save(modificatedCourse);
    }
}

