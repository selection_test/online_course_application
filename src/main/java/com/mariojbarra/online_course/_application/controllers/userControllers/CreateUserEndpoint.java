package com.mariojbarra.online_course._application.controllers.userControllers;

import com.mariojbarra.online_course._application.controllers.courseControllers.CourseRequestBody;
import com.mariojbarra.online_course._application.controllers.courseControllers.CourseService;
import com.mariojbarra.online_course._application.core.Course;
import com.mariojbarra.online_course._application.core.User;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")


    public class CreateUserEndpoint {
        private final UserService userService;

        public CreateUserEndpoint(UserService userService) {
            this.userService = userService;
        }

        @PostMapping
        public User createNewUser(@RequestBody UserRequestBody newUser){

            return userService.createUser(newUser);
        }


}
