package com.mariojbarra.online_course._application.controllers.userControllers;

import com.mariojbarra.online_course._application.core.Course;

import java.util.List;

public class UserRequestBody {
    private String userName;
    private List<Course> userCourses;


    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public List<Course> getUserCourses() {
        return userCourses;
    }

    public void setUserCourses(List<Course> userCourses) {
        this.userCourses = userCourses;
    }
}

