package com.mariojbarra.online_course._application.controllers.userControllers;

import com.mariojbarra.online_course._application.core.Course;
import com.mariojbarra.online_course._application.core.User;
import com.mariojbarra.online_course._application.core.UserRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User createUser(UserRequestBody newUser) {
        User userToCreate = new User();

        newUser.setUserName(userToCreate.getUserName());
        newUser.setUserCourses(Collections.emptyList());

        return userRepository.save(userToCreate);
    }

    public Optional<Object> getById(@PathVariable Long id) {

        return Optional.of(userRepository.findById(id));
    }

    public List<User> getAll() {
        List allCourses = new ArrayList<>(userRepository.findAll());

        return allCourses;
    }

    public void changeUser(User currentUser) {
        User userToChange = userRepository.findByUserName(currentUser.getUserName());
        userRepository.delete(userToChange);
        User modificatedUser = new User();
        userRepository.save(modificatedUser);

    }
}




