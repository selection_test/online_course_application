package com.mariojbarra.online_course._application.controllers.courseControllers;

import com.mariojbarra.online_course._application.core.Course;
import com.mariojbarra.online_course._application.core.Level;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController

public class GetCourseEndpoint {
    private final CourseService courseService;

    public GetCourseEndpoint(CourseService courseService) {
        this.courseService = courseService;
    }

    @GetMapping("/course")
    public List<Course> availableCourses() {

        return courseService.getAll();
    }

    @GetMapping("/course/{courseLevel}")
    public ResponseEntity<Course> coursesForLevel(@PathVariable("courseLevel") Level courseLevel) {


        Optional<Course> byLevel = courseService.getForLevel(courseLevel);

        if (byLevel.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        } else {
            Course forLevel = byLevel.get();
            return ResponseEntity.ok(forLevel);
        }
    }

    @GetMapping("/course/{courseName}")
    public ResponseEntity<Course> coursesForName(@PathVariable("courseName") String courseName) {


        Optional<Course> byName = Optional.ofNullable(courseService.getForName(courseName));

        if (byName.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        } else {
            Course forLevel = byName.get();
            return ResponseEntity.ok(forLevel);
        }
    }

    @PutMapping("/course/{courseName}")
    public void actualizar(@PathVariable("courseName") String currentName, @RequestBody Course currentCourse) {


        courseService.changeCourse(currentCourse);
    }
}
