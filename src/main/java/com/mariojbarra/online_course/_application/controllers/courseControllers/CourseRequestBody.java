package com.mariojbarra.online_course._application.controllers.courseControllers;

import com.mariojbarra.online_course._application.core.Level;
import com.mariojbarra.online_course._application.core.Status;

import java.time.LocalDate;
import java.util.Date;

public class CourseRequestBody {
    private String courseName;
    private Level courseLevel;
    private int courseDuration;
    private Status status;
    public LocalDate startDate;

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public Level getCourseLevel() {
        return courseLevel;
    }

    public void setCourseLevel(Level courseLevel) {
        this.courseLevel = courseLevel;
    }

    public int getCourseDuration() {
        return courseDuration;
    }

    public void setCourseDuration(int courseDuration) {
        this.courseDuration = courseDuration;
    }


    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }



}
