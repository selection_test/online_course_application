package com.mariojbarra.online_course._application.controllers.courseControllers;

import com.mariojbarra.online_course._application.core.Course;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/course")
public class CreateCourseEndpoint {
    private final CourseService courseService;

    public CreateCourseEndpoint(CourseService courseService) {
        this.courseService = courseService;
    }

    @PostMapping
    public Course createNewCourse(@RequestBody CourseRequestBody newCourse){
        return courseService.createCourse(newCourse);
    }
}
