### Requisitos funcionales
Se debe construir una api rest que cumpla con los siguientes requisitos funcionales:
* Como usuario, quiero acceder al catálogo de cursos disponibles en el sistema:

        ○ Sólo se mostrarán los cursos marcados como activos.
        ○ Se mostrará el título, el nivel y el número de horas.
        ○ Los cursos estarán ordenados alfabéticamente.

* Como usuario quiero dar de alta nuevos cursos, la información sobre los mismos será la siguiente:

      ○ Si está activo o no.
      ○ Título.
      ○ Número de horas, que debe ser un número entero positivo.
      ○ Nivel del curso, pudiendo seleccionar entre: básico, intermedio ya vanzado.
  
  * Como usuario quiero modificar cursos, para poder hacerlos inactivos.

### Extras:

Los cursos pueden listarse filtrados por nivel
Los cursos tendrán una fecha de inicio y fin, con lo cual un curso estará activo automáticamente si la fecha actual es anterior a su fecha de finalización.
